import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { BackendService } from './app.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [BackendService],
})
export class AppModule {}
