import { Controller, Get } from '@nestjs/common';
import { BackendService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly backendService: BackendService) {}

  @Get()
  patientList(): string {
    return JSON.stringify(this.backendService.patientList());
  }
}
