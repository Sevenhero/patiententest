export class Patient {
  id: number;
  firstName: string;
  lastName: string;
  birthday: Date;
  patientNumber?: number;
  city?: string;
  street?: string;
  houseNumber?: string;
  postCode?: number;
}
