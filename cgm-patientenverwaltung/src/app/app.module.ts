import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LayoutComponent } from "./layout/layout.component";
import { PatientenListeComponent } from "./patienten-liste/patienten-liste.component";
import { WebApiService } from "./web-api-service";
import { BackendService } from "./fakeBackend";

@NgModule({
  declarations: [AppComponent, LayoutComponent, PatientenListeComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [WebApiService, BackendService],
  bootstrap: [AppComponent]
})
export class AppModule {}
