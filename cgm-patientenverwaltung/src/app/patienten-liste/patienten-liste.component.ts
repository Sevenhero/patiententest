import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit
} from "@angular/core";
import { Patient } from "./patient.model";
import { WebApiService } from "../web-api-service";
import { NgForm } from "@angular/forms";
import { BackendService } from "../fakeBackend";

@Component({
  selector: "app-patienten-liste",
  templateUrl: "./patienten-liste.component.html",
  styleUrls: ["./patienten-liste.component.scss"]
})
export class PatientenListeComponent implements OnInit {
  constructor(
    private webApiService: WebApiService,
    private backendService: BackendService
  ) {}
  search: string = null;
  patients: Patient[] = [];
  editPatient: Patient = new Patient();

  @ViewChild("f") form: NgForm;

  async ngOnInit() {
    await this.load();
  }

  async load() {
    try {
      const patients = await this.backendService.patientList();
      this.patients = patients.slice();
    } catch (error) {
      console.error(error);
    }
  }

  lastSearch: string = null;
  async searchPatients() {
    try {
      this.patients = await this.backendService.searchPatients(this.search);
    } catch (error) {
      console.error(error);
    }
  }

  prepareEditPatient(clear: boolean = false) {
    this.editPatient = new Patient();
    this.editPatient.id = -1;
    this.error = null;
    if (clear) this.form.reset();
  }

  error: string = null;
  async onSubmit() {
    try {
      let errorString;
      if (this.editPatient.id >= 0)
        errorString = await this.backendService.updatePatient(this.editPatient);
      else
        errorString = await this.backendService.createPatient(this.editPatient);

      if (errorString == "") {
        this.prepareEditPatient(true);
        await this.load();
      } else {
        this.error = errorString;
      }
    } catch (error) {
      console.error(error);
    }
  }

  async editSelectedPatient(patientId: number) {
    this.editPatient = new Patient();
    try {
      this.editPatient = await this.backendService.getPatientById(patientId);
    } catch (error) {
      console.error(error);
    }
  }

  async deletePatient(patientId: number) {
    try {
      await this.backendService.deletePatient(patientId);
      await this.load();
    } catch (error) {
      console.error(error);
    }
  }

  parseDate(dateString: string): Date {
    if (dateString) {
      return new Date(dateString);
    } else {
      return null;
    }
  }
}
