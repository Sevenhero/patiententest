import { Patient } from "./patienten-liste/patient.model";
import { PatternValidator } from "@angular/forms";
import { createPureExpression } from "@angular/core/src/view/pure_expression";

export class BackendService {
  patientList(): Promise<Patient[]> {
    return Promise.resolve(this.dummyPatients);
  }

  updatePatient(patient: Patient) {
    let result = this.validatePatient(patient);
    if (result == "") this.updateSelectedPatient(patient);
    return Promise.resolve(result);
  }

  createPatient(patient: Patient): Promise<string> {
    let result = this.validatePatient(patient);
    if (result == "") this.createNewPatient(patient);
    return Promise.resolve(result);
  }

  private validatePatient(patient: Patient) {
    let result = "";
    let validInput = this.validateInput(patient);
    let isDuplicate = this.checkIsDuplicate(patient);
    if (!validInput) result += "Nicht alle eingaben waren gültig";
    if (isDuplicate)
      result += `${patient.firstName} ${
        patient.lastName
      } ist bereits im System vorhanden`;
    return result;
  }

  private checkIsDuplicate(patient: Patient) {
    return this.dummyPatients.some(
      p =>
        p.firstName == patient.firstName &&
        p.lastName == patient.lastName &&
        p.birthday.setHours(0, 0, 0, 0) ==
          new Date(patient.birthday).setHours(0, 0, 0, 0)
    );
  }

  private validateInput(patient: Patient) {
    return (
      this.validateString(patient.firstName) &&
      this.validateString(patient.lastName) &&
      patient.birthday != undefined &&
      patient.birthday != null
    );
  }

  private createNewPatient(patient: Patient) {
    patient.id =
      this.dummyPatients.length == 0
        ? 0
        : Math.max(...this.dummyPatients.map(p => p.id)) + 1;
    patient.birthday = new Date(patient.birthday);
    this.dummyPatients.push(patient);
  }

  private updateSelectedPatient(patient: Patient) {
    let indexOfPatientToBeUpdated = this.getIndexOfPatientByPatientId(
      patient.id
    );
    this.dummyPatients[indexOfPatientToBeUpdated] = patient;
  }

  getPatientById(patientId: number) {
    const patient = this.dummyPatients.find(p => p.id == patientId);
    return Promise.resolve(Object.create(patient));
  }

  private getIndexOfPatientByPatientId(patientId: number) {
    return this.dummyPatients.indexOf(
      this.dummyPatients.find(p => p.id == patientId)
    );
  }

  deletePatient(patientId: number) {
    const indexOfPatientToDelete = this.getIndexOfPatientByPatientId(patientId);
    this.dummyPatients.splice(indexOfPatientToDelete, 1);
    return Promise.resolve();
  }

  private validateString(value: string) {
    return value && value != null && value != undefined && value != "";
  }

  lastSearch: string;
  searchPatients(search: string) {
    let patients = [];
    if ((search || search == "") && this.lastSearch != search) {
      this.lastSearch = search;
      if (this.lastSearch == "") patients = this.dummyPatients.slice();
      else {
        let searchLowserCase = this.lastSearch.toLowerCase();
        let firstNames = this.dummyPatients.filter(p =>
          p.firstName.toLowerCase().includes(searchLowserCase)
        );
        let lastNames = this.dummyPatients.filter(p =>
          p.lastName.toLowerCase().includes(searchLowserCase)
        );
        for (const patient of firstNames) {
          if (lastNames.indexOf(patient) < 0) lastNames.push(patient);
        }
        patients = lastNames;
      }
      return Promise.resolve(patients);
    }
  }
  // // 2019-05-07
  // let dateAsString = `${patient.birthday.getFullYear()}-${patient.birthday.getMonth() +
  //   1}-${patient.birthday.getDate()}`;
  // (<any>this.editPatient.birthday) = dateAsString;

  private dummyPatients: Patient[] = [
    {
      id: 1,
      firstName: "Sven",
      lastName: "Kernke",
      birthday: new Date(1993, 6, 1),
      city: "Hardert",
      houseNumber: "14",
      patientNumber: 28273,
      postCode: 56579,
      street: "Auf Lischeid"
    }
  ];
}
