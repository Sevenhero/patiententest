import { HttpClient } from "@angular/common/http";
import { Patient } from "./patienten-liste/patient.model";
import { Injectable } from "@angular/core";

@Injectable()
export class WebApiService {
  constructor(private http: HttpClient) {}

  patientList(): Promise<string> {
    return this.http.get<string>("http://localhost:3000/patients").toPromise();
  }
}
